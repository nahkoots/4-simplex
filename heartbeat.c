/* Copyright 2021
 * This file is licensed under the GNU AGPL version 3.
 * See the LICENSE file.
 */

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>

#include "defs.h"
#include "config.h"
#include "player.h"

int printed_url = 0;

void heartbeat()
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(80);
	struct hostent *he = gethostbyname("www.classicube.net");
	memcpy(&addr.sin_addr, (struct in_addr *) he->h_addr_list[0], he->h_length);
	if (connect(sock, (struct sockaddr *) &addr, sizeof(addr)) == -1)
		printf("error\n");
	char req[250];
	sprintf(req, "GET /server/heartbeat?port=%d&name=%s&public=%s&version=7&salt=0&users=%d&max=%d&software=%s&web=True HTTP/1.1\r\nHost: www.classicube.net\r\n\r\n", conf.port, conf.name, (conf.ispublic? "True" : "False"), player_count(), conf.maxplayers, NAME);
	write(sock, req, strlen(req));
	
	// classicube.net will return a (the same) classicube server address every time it receives a heartbeat
	// this parses the returned address from the first heartbeat response and prints it
	if (!printed_url) {
		printed_url ++;
		printf("classicube address: ");
		fcntl(sock, F_SETFL, fcntl(sock, F_GETFL, 0) | O_NONBLOCK);
		char buf[1000];
		int size = 0;
		char http_end[5] = "0\r\n\r\n";
		
		while (size < 5 || memcmp(buf + size - 5, http_end, 5)) {
			if (read(sock, buf + size, 1) > 0) {
				size ++;
			}
		}
		// the response looks like "[HTTP header info]\r\nhttp://www.classicube.net/server/play/[identifier]/\r\n0\r\n\r\n"
		// this terminates the string immediately after the last "/"
		while (buf[size] != '/') {
			size --;
		}
		buf[size + 1] = 0;
		printf("%s\n", strstr(buf, "http://www.classicube.net"));
	}
	
	close(sock);
	
	// http://betacraft.pl/heartbeat.jsp?port=25565&name=TestServer&public=True&version=7&salt=0&users=2&max=10
}
