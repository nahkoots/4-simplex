/* Copyright 2021
 * This file is licensed under the GNU AGPL version 3.
 * See the LICENSE file.
 */

#ifndef LEVEL_H
#define LEVEL_H
#include "packets.h"


struct level
{
	char *fname;
	char *name;
	char *uuid;
	int changed;

	short_type x, y, z;
	byte_type *data;
	byte_type *apparent;
	int32_t size;

	short_type spawn_x, spawn_y, spawn_z;
	byte_type spawn_yaw, spawn_pitch;

	struct block **blockdata;
};

extern struct alist levels;
extern struct level main_lvl;

struct player;

void lvl_send_main(struct player *p);
void lvl_send(struct player *p, struct level *l);
void lvl_set_block(struct level *l, short_type x, short_type y, short_type z,
					byte_type block);
void lvl_send_block(struct level *l, short_type x, short_type y, short_type z,
					byte_type block);
void lvl_set_block_nosend(struct level *l, short_type x, short_type y, short_type z,
					byte_type block);
void lvl_swap_block(struct level *l, short_type x0, short_type y0, short_type z0, 
					short_type x1, short_type y1, short_type z1);
void lvl_swap_block_nosend(struct level *l, short_type x0, short_type y0, short_type z0, 
					short_type x1, short_type y1, short_type z1);
byte_type lvl_get_block(struct level *l, short_type x, short_type y, short_type z); // returns block at x, y, z, or -1 if out of bounds
int lvl_load(struct level *l, char *fname);
int lvl_save(struct level *l);
int lvl_gen_flat(struct level *l, char *fname, int x, int y, int z);
int lvl_blocks_init(struct level *l);
int lvl_block_init(struct level *l, short_type x, short_type y, short_type z);

#endif
