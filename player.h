/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version.
 */

#ifndef PLAYER_H
#define PLAYER_H
#include <stdio.h>
#include <sys/select.h>
#include <pthread.h>
#include "utils.h"
// #include "packets.h"
#include "level.h"

struct player
{
	char name[65];
	sbyte_type id;
	struct alist groups;

	// stored in network byte order, low five bits fractional
	short_type x, y, z;

	byte_type yaw, pitch;
	struct level *level;

	enum {STATE_BLOCK_NORMAL,
		  STATE_BLOCK_CUBOID,
		  STATE_BLOCK_BLOCK,
		  STATE_BLOCK_GETTEMP} state_block;
	int cuboid_x, cuboid_y, cuboid_z;
	byte_type block_id;

	enum {STATE_CHAT_NORMAL, STATE_CHAT_PM} state_chat;
	int pm_id;

	int connected;
	int finalized;
	byte_type buf[1024]; //[131];
	int buf_size;
	int fd;
	int protocol; // 0 for standard, 1 for websocket

	char *pmsg; // persistent message, used to store appended messages
	int num_extensions; // number of CPE extensions to wait for
	int num_declared; // track extensions already declared
	pthread_mutex_t fd_write_mutex; // prevents trying to write 2 packets at once
	
	int supports_LongerMessages;
};

extern struct llist players;
extern pthread_rwlock_t player_list_mutex;
extern pthread_rwlock_t lock_rw;

extern fd_set master_fds;
extern fd_set read_fds;

struct player *player_get_fd(int fd);
void player_send(struct player *p, const void *buf, int count);
void player_disconnect(struct player *p, char *reason);
void player_move(struct player *p, short_type x, short_type y, short_type z,
				 byte_type yaw, byte_type pitch);
void player_tp(struct player *p, short_type x, short_type y, short_type z,
			   byte_type yaw, byte_type pitch);
void player_send_move(struct player *p, struct player *moving,
					  int x, int y, int z, int yaw, int pitch);
void player_msg(struct player *p, const char *msg);
void player_msgf(struct player *p, char *fmt, ...);
void broadcast_msg(const char *msg);
void broadcast_msgf(char *fmt, ...);
void player_init_ids();
sbyte_type player_get_id();
void player_set_block(struct player *p,
					  short_type x, short_type y, short_type z,
					  byte_type mode, byte_type block);
void player_despawn(struct player *p, struct player *a);
void player_spawn(struct player *p, struct player *a);
struct alist player_find(char *name);
int player_find_one(struct player *p, struct player **q, char *name);
int player_count();
void player_block_switch(struct player *p);

#endif
