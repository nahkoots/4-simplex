4-simplex
===
4-simplex is a fast Minecraft Classic server software with multithreaded realistic physics.

Instructions
===
Install your distro's version of the sqlite3 development package.  

Copy config.json.example to config.json.  
Modify config.json as desired.  
Run ```db-init.sh```. Provide the administrator's username as the argument.  
Run ```make```.  
Run the 4-simplex executable.  

Usage
===
4-simplex uses groups to manage permissions, rather than the traditional rank-based approach. 

There are two fundamental parts of the group system. The first is a permission, which is, abstractly, a license to do one specific thing. For example, the "cuboid" permission allows a player to use /z, and the "save" permission allows a player to save the map. The second is a group, which is a collection of permissions. A group can contain any number of permissions, and a permission can be a member of several groups. A player can belong to an arbitrary number of groups, and when a player belongs to a group, they are granted all of its permissions.

The only default group (aside from any additional defaults specified in config.json) is "all", which contains all of the permissions needed to use the group management system. The administrator (provided above) is a member of this group. The group management commands are as follows:
* ```group info <group>```: lists all of the permissions in group
* ```group list```: lists all of the server's groups
* ```group list <player>```: lists all of the groups player is a member of
* ```group players <group>```: lists all members of group
* ```group set <player> <group>```: adds player to group
* ```group unset <player> <group>```: removes player from group
* ```group add <group> <permission>```: adds permission to group
* ```group remove <group> <permission>```: removes permission from group
* ```group create <group>```: creates an empty group
* ```group delete <group>```: deletes a group
