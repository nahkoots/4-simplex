/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

CREATE TABLE Player
(
    id INTEGER PRIMARY KEY,
    name TEXT
);

CREATE TABLE PGroup
(
    id INTEGER PRIMARY KEY,
    name TEXT
);

CREATE TABLE Permission
(
    pgroup INTEGER,
    name TEXT,
    FOREIGN KEY (pgroup) REFERENCES PGroup(id) ON DELETE CASCADE,
    PRIMARY KEY (pgroup, name)
);

CREATE TABLE PlayerGroup
(
    player INTEGER,
    pgroup INTEGER,
    FOREIGN KEY (player) REFERENCES Player(id),
    FOREIGN KEY (pgroup) REFERENCES PGroup(id) ON DELETE CASCADE,
    PRIMARY KEY (player, pgroup)
);

INSERT INTO PGroup(name)
VALUES ('all');

INSERT INTO PGroup(name)
VALUES ('default');

INSERT INTO Permission(pgroup, name)
SELECT PGroup.id, 'group-all'
FROM PGroup
WHERE Pgroup.name = 'all';

