/* Copyright 2021
 * This file is licensed under the GNU AGPL version 3.
 * See the LICENSE file.
 */

#ifndef BLOCK_H
#define BLOCK_H

#include <stdio.h>
#include <sys/select.h>

#include "utils.h"
#include "packets.h"
#include "level.h"

#define DEF_T 297
#define DEF_D 5
#define DEF_K 1000
#define DEF_H 10
#define MAX_T 9999

#define AIR 0
#define STONE 1
#define GRASS 2
#define DIRT 3
#define COBBLE 4
#define PLANK 5
#define SAPLING 6
#define BEDROCK 7
#define WATER 8
#define STILL_WATER 9
#define LAVA 10
#define STILL_LAVA 11
#define SAND 12
#define GRAVEL 13
#define GOLD_ORE 14
#define IRON_ORE 15
#define COAL_ORE 16
#define WOOD 17
#define LEAVES 18
#define SPONGE 19
#define GLASS 20
#define RED 21
#define ORANGE 22
#define YELLOW 23
#define LIME 24
#define GREEN 25
#define TEAL 26
#define AQUA 27
#define CYAN 28
#define BLUE 29
#define INDIGO 30
#define VIOLET 31
#define MAGENTA 32
#define PINK 33
#define BLACK 34
#define GRAY 35
#define WHITE 36
#define DANDELION 37
#define ROSE 38
#define BROWN_SHROOM 39
#define RED_SHROOM 40
#define GOLD 41
#define IRON 42
#define DOUBLE_SLAB 43
#define SLAB 44
#define BRICK 45
#define TNT 46
#define BOOKS 47
#define MOSSY_COBBLE 48
#define OBSIDIAN 49
#define COBBLE_SLAB 50
#define ROPE 51
#define SANDSTONE 52
#define SNOW 53
#define FIRE 54
#define LIGHT_PINK 55
#define FOREST_GREEN 56
#define BROWN 57
#define DEEP_BLUE 58
#define TURQUOISE 59
#define ICE 60
#define CERAMIC 61
#define MAGMA 62
#define PILLAR 63
#define CRATE 64
#define STONE_BRICK 65

struct block {
	byte_type base; // the original block
	int temperature;
	int energy;			// current energy
	int energy_next_diff;	// what the change in energy will be in next tick
};

enum states {
	STATE_MATTER_GAS,
	STATE_MATTER_LIQUID,
	STATE_MATTER_SOLID,
	STATE_MATTER_POWDER,	// melts into liquid, does not return to powder
	STATE_MATTER_POWDER2,	// same as above, but falls upward
	STATE_MATTER_PLANT	// burns at melting point
};

// returns defaults for a given block type
int block_get_default_temperature(byte_type id);	// K
int block_get_density(byte_type id);     // kg/m^3
int block_get_default_thermal_conductivity(byte_type id);			// J/tick
int block_get_default_specific_heat(byte_type id);
enum states block_get_default_state (byte_type id);

// returns block data for a specific block
int block_get_specific_heat(struct block *b);
int block_get_melting_point(struct block *b);
int block_get_boiling_point(struct block *b);
byte_type block_get_phase_solid(struct block *b);
byte_type block_get_phase_liquid(struct block *b);
byte_type block_get_phase_gas(struct block *b);


// basic physical attributes for all block types
static const struct block_defaults {
	int temperature;             // K
	int density;                 // kg/m^3
	int thermal_conductivity;    // J/sec
	int specific_heat;           // J/(K cm^3) (volumetric; largely unrealistic)
	enum states state;
	int melting_point;
	int boiling_point;
	byte_type phase_solid;
	byte_type phase_liquid;
	byte_type phase_gas;
} block_default_data[] = {
	{DEF_T, DEF_D, 0,     DEF_H, STATE_MATTER_GAS,    0,     0,     AIR,         AIR,      AIR}, // 0 - air
	{DEF_T, DEF_D, DEF_K, 10,    STATE_MATTER_SOLID,  1023,  MAX_T, STONE,       LAVA,     AIR}, // 1 - stone
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, DIRT,        MAGMA,    AIR}, // 2 - grass
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, DIRT,        MAGMA,    AIR}, // 3 - dirt
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, COBBLE,      MAGMA,    AIR}, // 4 - cobble
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_PLANT,  573,   MAX_T, PLANK,       COAL_ORE, AIR}, // 5 - plank
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_PLANT,  573,   MAX_T, SAPLING,     AIR,      AIR}, // 6 - sapling
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  MAX_T, MAX_T, BEDROCK,     AIR,      AIR}, // 7 - bedrock
	{DEF_T, DEF_D, DEF_K, 200,   STATE_MATTER_LIQUID, 273,   373,   ICE,         WATER,    SANDSTONE}, // 8 - water
	{DEF_T, DEF_D, DEF_K, 200,   STATE_MATTER_SOLID,  1,     MAX_T, STILL_WATER, AIR,      AIR}, // 9 - still water (water source)
	{4173,  DEF_D, DEF_K, 100,   STATE_MATTER_LIQUID, 1023,  MAX_T, OBSIDIAN,    LAVA,     AIR}, // 10 - lava
	{4173,  DEF_D, DEF_K, 100,   STATE_MATTER_SOLID,  MAX_T, MAX_T, STILL_LAVA,  AIR,      AIR}, // 11 - still lava (lava source)
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_POWDER, 1073,  MAX_T, GLASS,       LAVA,     AIR}, // sand
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_POWDER2, 1023,  MAX_T, COBBLE,      LAVA,     AIR}, // gravel
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, GOLD,        LAVA,     AIR},	// gold ore
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, IRON,        LAVA,     AIR},	// iron ore
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_PLANT,  1023,  MAX_T, COAL_ORE,    AIR,      AIR},	// coal ore
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_PLANT,  1023,  MAX_T, WOOD,        AIR,      AIR},	// wood
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_PLANT,  1023,  MAX_T, LEAVES,      AIR,      AIR},	// leaves
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 0, 0},	// sponge
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 62, 0},	// glass
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 62, 0},	// red
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 62, 0},	// orange
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// yellow
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// lime
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// green
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// teal
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// aqua
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// cyan
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// blue
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// indigo
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// violet
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// magenta
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// pink
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// black
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// gray
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// white
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// dandelion
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// rose
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// brown shroom
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// red shroom
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// gold
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// iron
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// double slab
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// slab
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// brick
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// tnt
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// books
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// mossy
	{DEF_T, DEF_D, DEF_K, 90,    STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// obsidian
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// cobble slab
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// rope
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_GAS,  10,  11, 1, 10, 0},	// sandstone
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// snow
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// fire
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// light pink
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// forest green
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// brown
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// deep blue
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// turquoise
	{250,   DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  273,   373, 60, 8, 0},	// ice
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// ceramic
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_LIQUID, 1023,  MAX_T, 1, 10, 0},	// magma
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// pillar
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// crate
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// stone brick
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// water source
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// lava source
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	// steam
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
	{DEF_T, DEF_D, DEF_K, DEF_H, STATE_MATTER_SOLID,  1023,  MAX_T, 1, 10, 0},	//
};

#endif
