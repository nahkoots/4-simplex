.RECIPEPREFIX := >

objects = main.o command.o config.o db.o heartbeat.o level.o nbt.o packets.o\
player.o utils.o block.o physics.o

objects += pdjson.o

CFLAGS = -g -std=gnu99 -Wall -Wextra -Wpedantic -D_GNU_SOURCE

pdjson.o: CFLAGS += -Wno-implicit-fallthrough

all: $(objects)
>   $(CC) -O3 -pthread -o 4-simplex $(objects) -lz -lssl -lcrypto -lsqlite3
main.o: packets.h player.h heartbeat.h level.h command.h utils.h config.h physics.h
player.o: packets.h level.h utils.h
level.o: player.h nbt.h block.h
command.o: utils.h
config.o: pdjson.h
physics.o: block.h

.PHONY: clean
clean:
>   rm -f 4-simplex $(objects)

.PHONY: db
db: playerdata.db
playerdata.db:
ifndef op
>   $(warning warning: no op specified)
endif
>   ./db-init.sh $(op)

