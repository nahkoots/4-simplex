/* Copyright 2022
 * This file is licensed under the GNU AGPL version 3.
 * See the LICENSE file.
 */

#ifndef PHYSICS_H
#define PHYSICS_H
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <assert.h>
#include <limits.h>
#include <sys/time.h>

#include "level.h"

struct physics {
	struct level *level;
	struct stack *coord_stack;
};

struct coords {
	short_type x, y, z;
	byte_type direction;
	byte_type *valid;
};

struct helper_data {
	pthread_mutex_t *list_access_mutex;
	struct llist *region_list;
	struct level *level;
};

struct region {
	// bounding box for the region (inclusive)
	int x_start, y_start, z_start;
	int x_end, y_end, z_end;
	// physics number for blocks in this region
	int identifier;
};

// constantly runs block movement physics for a level (call with pthread)
void *do_movement(void *data);
// constantly runs block temperature physics for a level (call with pthread)
void *do_temperature(void *data);
// starts the do_physics cycle in a new thread
int begin_physics(struct level *level);
// handles physics in a more realistic manner
void handle_physics_realistic(struct level *level, int x, int y, int z);
void handle_physics_realistic2(struct level *level, int x, int y, int z);
// calculates what the energy of all blocks will be in the next tick
void handle_temperature_pass1(struct level *level, int x, int y, int z);
// applies the changes calculated above, recalculates and stores new temp
void handle_temperature_pass2(struct level *level, int x, int y, int z);
// gets the block at x, y, z given a level array formatted byte type array pointer
void handle_state_change(struct level *level, int x, int y, int z);
byte_type data_get_block(byte_type *data, int x, int y, int z);


#endif
