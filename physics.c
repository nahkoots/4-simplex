/* Copyright 2022
 * This file is licensed under the GNU AGPL version 3.
 * See the LICENSE file.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <assert.h>
// #include <pthread.h>
#include <limits.h>
#include <sys/time.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <arpa/inet.h>

#include "defs.h"
#include "utils.h"
#include "db.h"
#include "physics.h"
#include "level.h"
#include "block.h"

#include "command.h"

// Uncomment to calculate particle motion deterministically.
// Causes powder movement to be directionally biased.
#define NOT_RANDOM

// Sets how many pthreads get created to process regions.
#define NUM_HELPER_THREADS 4

#define blockat(a, b, c) (b)*level->x*level->z+(c)*level->x+(a)+4
#define blockat2(a, b, c) (b)*level->x*level->z+(c)*level->x+(a)
#define inbounds(a, b, c) a >= 0 && b >= 0 && c >= 0 && a < level->x && b < level->y && c < level->z

void *process_physics_region(void *data);
void calculate_energy_transfer(struct level *level, int x0, int x1, int y0, int y1, int z0, int z1);
void *assign_physics_nums(struct level *level, struct region ***physics_regions);
void *send_changes(void *changes);

//void handle_liquid_realistic(struct level *level, int x, int y, int z, byte_type *processed, struct stack *coord_stack);
//void handle_liquid_realistic2(struct level *level, int x, int y, int z, byte_type *processed, struct stack *coord_stack);
int8_t handle_helper(struct level *level, struct llist *donors, struct llist *down_recipients, struct llist *side_recipients, int x, int y, int z, byte_type *processed, byte_type *valid);



int begin_physics(struct level *level) {
	pthread_t physics_thread;
	struct physics *data = malloc(sizeof(struct physics));
	data->level = level;
	printf("starting physics: %d\n", pthread_create(&physics_thread, NULL, do_movement, data));
	return 1;
}

void *do_movement(void *d) {
	struct physics *data = (struct physics *)d;
	struct level *level = data->level;
	struct region ***physics_regions = calloc(level->x*level->y*level->z, sizeof(struct region *));
	pthread_t change_send_thread;
	
	struct timeval t1, t2;
	// struct timeval t2_1;
	gettimeofday(&t1, NULL);
	gettimeofday(&t2, NULL);
	
	// want helper threads to get jobs from the list one at a time
	pthread_mutex_t *list_access_mutex = malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(list_access_mutex, NULL);
	
	struct llist *region_list = malloc(sizeof(struct llist));
	llist_init(region_list);
	
	struct helper_data *helper_data = malloc(sizeof(struct helper_data));
	helper_data->list_access_mutex = list_access_mutex;
	helper_data->region_list = region_list;
	helper_data->level = level;
	
	pthread_t region_threads[NUM_HELPER_THREADS];
	for (int i = 0; i < NUM_HELPER_THREADS; i ++) {
		pthread_create(&region_threads[i], NULL, process_physics_region, helper_data);
	}
//	printf("malloced %d\n", level->x*level->y*level->z + 4);
//	printf("level size is %d %d %d\n", level->x, level->y, level->z);
//	printf("blockat size is %d", blockat(level->x, level->y, level->z));
	
	while (1) {
		gettimeofday(&t2, NULL);
		/*
		t2_1.tv_sec = t2.tv_sec - t1.tv_sec;
		t2_1.tv_usec = t2.tv_usec - t1.tv_usec;
		if (t2_1.tv_usec < 0) {
			--t2_1.tv_sec;
			t2_1.tv_usec += 1000000;
		}
		if (t2_1.tv_sec < 1) {
			//continue;
		}
		 * */

		// handle movement
		// handle temperature physics
		// handle temperature-related state changes
//		memcpy(old_blocks, level->data, (level->x*level->y*level->z + 4));
		assign_physics_nums(level, physics_regions);
		for (int j = 0; j < level->y; j ++) {
			for (int i = 0; i < level->x; i ++) {
				for (int k = 0; k < level->z; k ++) {
					handle_physics_realistic(level, i, j, k);
				}
			}
		}
		for (int j = level->y - 1; j >= 0; j --) {
			for (int i = level->x - 1; i >= 0; i --) {
				for (int k = level->z - 1; k >= 0; k --) {
					handle_physics_realistic2(level, i, j, k);
				}
			}
		}
		gettimeofday(&t1, NULL);
		// printf("done (took %ld microseconds)\n", ((t1.tv_sec - t2.tv_sec)*1000000L +t1.tv_usec) - t2.tv_usec);
		// calculate changes and update users
		void *ret = 0;
		if (change_send_thread) {
			pthread_join(change_send_thread, ret);
		}
		pthread_create(&change_send_thread, NULL, send_changes, level);
		
	}
	return NULL;
}

//struct helper_data {
//	pthread_mutex_t *list_access_mutex;
//	struct llist *region_list;
//	struct level *level;
//};

//struct region {
//	// bounding box for the region (inclusive)
//	int x_start, y_start, z_start;
//	int x_end, y_end, z_end;
//	// physics number for blocks in this region
//	int identifier;
//};

void *process_physics_region(void *data) {
	struct helper_data local_data;
	memcpy(&local_data, data, sizeof(struct helper_data));
	pthread_mutex_t *list_access_mutex = local_data.list_access_mutex;
	while (1) {
		pthread_mutex_lock(list_access_mutex);
		if (local_data.region_list->first == NULL) {
			continue;
		}
		struct llist_item *region_container = (local_data.region_list)->last;
		struct region my_region;
		memcpy(&my_region, region_container->val, sizeof(struct region));
		llist_remove(local_data.region_list, region_container);
		pthread_mutex_unlock(list_access_mutex);
		
		int x_start = my_region.x_start;
		int y_start = my_region.y_start;
		int z_start = my_region.z_start;
		
		int x_end = my_region.x_end;
		int y_end = my_region.y_end;
		int z_end = my_region.z_end;
		
		struct level *level = local_data.level;
		
		for (int j = y_start; j <= y_end; j ++) {
			for (int i = x_start; i <= x_end; i ++) {
				for (int k = z_start; k < z_end; k ++) {
					handle_physics_realistic(level, i, j, k);
				}
			}
		}
		for (int j = level->y - 1; j >= 0; j --) {
			for (int i = level->x - 1; i >= 0; i --) {
				for (int k = level->z - 1; k >= 0; k --) {
					handle_physics_realistic2(level, i, j, k);
				}
			}
		}
	}
}

void *assign_physics_nums(struct level *level, struct region ***physics_regions) {
	int current_region = 0;
	for (int j = 0; j < level->y; j ++) {
		for (int i = 0; i < level->x; i ++) {
			for (int k = 0; k < level->z; k ++) {
				struct region **region;
				int has_prev = 0, had_any = 0;
				
				if (inbounds(i - 1, j, k) &&
						block_get_default_state(level->data[blockat2(i - 1, j, k)]) == block_get_default_state(level->data[blockat2(i, j, k)])) {
					region = physics_regions[blockat2(i - 1, j, k)];
					has_prev = 1;
					had_any = 1;
				}
				if (inbounds(i, j - 1, k) &&
						block_get_default_state(level->data[blockat2(i, j - 1, k)]) == block_get_default_state(level->data[blockat2(i, j, k)])) {
					if (has_prev) {
						struct region **temp = physics_regions[blockat2(i, j - 1, k)];
						if (temp != region) {
							free(temp);
						}
						*physics_regions[blockat2(i, j - 1, k)] = *region;
					}
					region = physics_regions[blockat2(i, j - 1, k)];
					has_prev = 1;
					had_any = 1;
				}
				if (inbounds(i, j, k - 1) &&
						block_get_default_state(level->data[blockat2(i, j, k - 1)]) == block_get_default_state(level->data[blockat2(i, j, k)])) {
					if (has_prev) {
						struct region **temp = physics_regions[blockat2(i, j, k - 1)];
						if (temp != region) {
							free(temp);
						}
						*physics_regions[blockat2(i, j, k - 1)] = *region;
					}
					region = physics_regions[blockat2(i, j, k - 1)];
					had_any = 1;
				} 
				if (!had_any) {
					current_region ++;
					
					struct region **region_ptr = malloc(sizeof(void *));
					*region_ptr = malloc(sizeof(struct region));
					
					struct region *new_region = *region_ptr;
					new_region->x_start = new_region->x_end = i;
					new_region->y_start = new_region->y_end = j;
					new_region->z_start = new_region->z_end = k;
					
				}
				
				
				physics_regions[blockat2(i, j, k)] = region;
			}
		}
	}
	printf("%d\n", current_region);
	return 0;
}

void *send_changes(void *data) {
	struct level *level = (struct level*)data;
	
	for(int j = 0; j < level->y; j++) {
		for(int i = 0; i < level->x; i++) {
			for(int k = 0; k < level->z; k++) {
				byte_type block = level->data[blockat (i, j, k)];
				if (level->apparent[blockat (i, j, k)] != block) {
					lvl_send_block(level, i, j, k, block);
				}
			}
		}
	}
	return 0;
}

void handle_physics_realistic(struct level *level, int x, int y, int z) {
	int possible[6];
	int possibleIndex = 0;

	switch (block_get_default_state(level->data[blockat(x, y, z)])) {
	case STATE_MATTER_GAS:;
		byte_type this_block = level->data[blockat(x, y, z)];
		if (inbounds(x - 1, y, z) &&
			level->data[blockat(x - 1, y, z)] != this_block &&
			block_get_default_state(level->data[blockat(x - 1, y, z)]) == STATE_MATTER_GAS) {
			possible[possibleIndex] = 0;
			possibleIndex ++;
		}
		if (inbounds(x + 1, y, z) &&
			level->data[blockat(x + 1, y, z)] != this_block &&
			block_get_default_state(level->data[blockat(x + 1, y, z)]) == STATE_MATTER_GAS) {
			possible[possibleIndex] = 1;
			possibleIndex ++;
		}
		if (inbounds(x, y - 1, z) &&
			level->data[blockat(x, y - 1, z)] != this_block &&
			block_get_default_state(level->data[blockat(x, y - 1, z)]) == STATE_MATTER_GAS) {
			possible[possibleIndex] = 2;
			possibleIndex ++;
		}
		if (inbounds(x, y + 1, z) &&
			level->data[blockat(x, y + 1, z)] != this_block &&
			block_get_default_state(level->data[blockat(x, y + 1, z)]) == STATE_MATTER_GAS) {
			possible[possibleIndex] = 3;
			possibleIndex ++;
		}
		if (inbounds(x, y, z - 1) &&
			level->data[blockat(x, y, z - 1)] != this_block &&
			block_get_default_state(level->data[blockat(x, y, z - 1)]) == STATE_MATTER_GAS) {
			possible[possibleIndex] = 4;
			possibleIndex ++;
		}
		if (inbounds(x, y, z + 1) &&
			level->data[blockat(x, y, z + 1)] != this_block &&
			block_get_default_state(level->data[blockat(x, y, z + 1)]) == STATE_MATTER_GAS) {
			possible[possibleIndex] = 5;
			possibleIndex ++;
			
		}
		
		if (possibleIndex > 0) {
#ifdef NOT_RANDOM
			int r = possible[possibleIndex - 1];
#else				
			int r = possible[random() % possibleIndex];
#endif		
			switch (r) {
				case 0:
					lvl_swap_block_nosend(level, x, y, z, x - 1, y, z);
					break;
				case 1:
					lvl_swap_block_nosend(level, x, y, z, x + 1, y, z);
					break;
				case 2:
					lvl_swap_block_nosend(level, x, y, z, x, y - 1, z);
					break;
				case 3:
					lvl_swap_block_nosend(level, x, y, z, x, y + 1, z);
					break;
				case 4:
					lvl_swap_block_nosend(level, x, y, z, x, y, z - 1);
					break;
				case 5:
					lvl_swap_block_nosend(level, x, y, z, x, y, z + 1);
					break;
				default:
					break;
			}
		}
		break;
	case STATE_MATTER_LIQUID:
		break;
	case STATE_MATTER_SOLID:
		break;
	case STATE_MATTER_POWDER:
		if (inbounds(x, y - 1, z) && block_get_default_state(level->data[blockat(x, y - 1, z)]) == STATE_MATTER_GAS) {
			possibleIndex = 1;
			lvl_swap_block_nosend(level, x, y, z, x, y - 1, z);

		}
		if (possibleIndex == 0) { //  && block_get_default_state(level->data[blockat(x, y - 1, z)]) == STATE_MATTER_POWDER
			if (inbounds(x - 1, y - 1, z) && block_get_default_state(level->data[blockat(x - 1, y - 1, z)]) == STATE_MATTER_GAS) {
				possible[0] = 0;
				possibleIndex ++;
			}
			if (inbounds(x + 1, y - 1, z) && block_get_default_state(level->data[blockat(x + 1, y - 1, z)]) == STATE_MATTER_GAS) {
				possible[possibleIndex] = 1;
				possibleIndex ++;
			}
			if (inbounds(x, y - 1, z - 1) && block_get_default_state(level->data[blockat(x, y - 1, z - 1)]) == STATE_MATTER_GAS) {
				possible[possibleIndex] = 2;
				possibleIndex ++;
			}
			if (inbounds(x, y - 1, z + 1) && block_get_default_state(level->data[blockat(x, y - 1, z + 1)]) == STATE_MATTER_GAS) {
				possible[possibleIndex] = 3;
				possibleIndex ++;
			}
			if (possibleIndex > 0) {
				// byte_type temp;

#ifdef NOT_RANDOM
				int r = possible[possibleIndex - 1];
#else				
				int r = possible[random() % possibleIndex];
#endif				
				switch (r) {
				case 0:
					lvl_swap_block_nosend(level, x, y, z, x - 1, y - 1, z);
					break;
				case 1:
					lvl_swap_block_nosend(level, x, y, z, x + 1, y - 1, z);
					break;
				case 2:
					lvl_swap_block_nosend(level, x, y, z, x, y - 1, z - 1);
					break;
				case 3:
					lvl_swap_block_nosend(level, x, y, z, x, y - 1, z + 1);
					break;
				default:
					break;
				}
			}
		}
		break;
	case STATE_MATTER_PLANT:
		break;
		default:
	break;
	}
	return;
}
	
void handle_physics_realistic2(struct level *level, int x, int y, int z) {
	int possible[6];
	int possibleIndex = 0;

	switch (block_get_default_state(level->data[blockat(x, y, z)])) {
		case STATE_MATTER_POWDER2:
		if (inbounds(x, y + 1, z) && block_get_default_state(level->data[blockat(x, y + 1, z)]) == STATE_MATTER_GAS) {
			possibleIndex = 1;
			lvl_swap_block_nosend(level, x, y, z, x, y + 1, z);

		}
		if (possibleIndex == 0) { //  && block_get_default_state(level->data[blockat(x, y - 1, z)]) == STATE_MATTER_POWDER
			if (inbounds(x - 1, y + 1, z) && block_get_default_state(level->data[blockat(x - 1, y + 1, z)]) == STATE_MATTER_GAS) {
				possible[0] = 0;
				possibleIndex ++;
			}
			if (inbounds(x + 1, y + 1, z) && block_get_default_state(level->data[blockat(x + 1, y + 1, z)]) == STATE_MATTER_GAS) {
				possible[possibleIndex] = 1;
				possibleIndex ++;
			}
			if (inbounds(x, y + 1, z - 1) && block_get_default_state(level->data[blockat(x, y + 1, z - 1)]) == STATE_MATTER_GAS) {
				possible[possibleIndex] = 2;
				possibleIndex ++;
			}
			if (inbounds(x, y + 1, z + 1) && block_get_default_state(level->data[blockat(x, y + 1, z + 1)]) == STATE_MATTER_GAS) {
				possible[possibleIndex] = 3;
				possibleIndex ++;
			}
			if (possibleIndex > 0) {
				// byte_type temp;

#ifdef NOT_RANDOM
				int r = possible[possibleIndex - 1];
#else				
				int r = possible[random() % possibleIndex];
#endif		
				switch (r) {
				case 0:
					lvl_swap_block_nosend(level, x, y, z, x - 1, y + 1, z);
					break;
				case 1:
					lvl_swap_block_nosend(level, x, y, z, x + 1, y + 1, z);
					break;
				case 2:
					lvl_swap_block_nosend(level, x, y, z, x, y + 1, z - 1);
					break;
				case 3:
					lvl_swap_block_nosend(level, x, y, z, x, y + 1, z + 1);
					break;
				default:
					break;
				}
			}
		}
		break;
		default:
			break;
	}
}