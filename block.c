/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version.
 */

#include <stdarg.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include "packets.h"
#include "command.h"
#include "block.h"


int block_get_default_temperature(byte_type id) {
	return block_default_data[id].temperature;
}

int block_get_default_density(byte_type id) {
	return block_default_data[id].density;
}

int block_get_default_thermal_conductivity(byte_type id) {
	return block_default_data[id].thermal_conductivity;
}

int block_get_default_specific_heat(byte_type id) {
	return block_default_data[id].specific_heat;
}

int block_get_specific_heat(struct block *b) {
	return block_default_data[b->base].specific_heat;
}

int block_get_melting_point(struct block *b) {
	return block_default_data[b->base].melting_point;
}

int block_get_boiling_point(struct block *b) {
	return block_default_data[b->base].boiling_point;
}

byte_type block_get_phase_solid(struct block *b) {
	return block_default_data[b->base].phase_solid;
}

byte_type block_get_phase_liquid(struct block *b) {
	return block_default_data[b->base].phase_liquid;
}

byte_type block_get_phase_gas(struct block *b) {
	return block_default_data[b->base].phase_gas;
}

enum states block_get_default_state(byte_type id) {
	return block_default_data[id].state;
}
